<?php

/**
 * @file
 * Contains Drupal\awssdk\Form\ConfigurationForm.
 */

namespace Drupal\awssdk\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigurationForm.
 *
 * @package Drupal\awssdk\Form
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'awssdk.configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('awssdk.configuration');
    $form = parent::buildForm($form, $form_state);

    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . t('It may be a security risk to store your Amazon AWS keys and secret key in the database. Anyone who has access to your database will have access to this information. Consider using the <a href=":key_url">Key</a> module to protect this data.', [':key_url' => 'https://www.drupal.org/project/key']),
    ];
    $form['required'] = array(
      '#type' => 'fieldset',
      '#title' => t('Required'),
      '#description' => t('The following fields are required by the SDK, but may be passed directly to the constructors instead. The second two fields have acceptable defaults so you only need to fill in the first two.'),
    );
    $form['required']['aws_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Access Key ID'),
      '#default_value' => $config->get('aws_key'),
      '#required' => TRUE,
      '#description' => t('Amazon Web Services Key. Found in the AWS Security Credentials.'),
    );
    $form['required']['aws_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('Secret Access Key'),
      '#default_value' => $config->get('aws_secret'),
      '#required' => TRUE,
      '#description' => t('Amazon Web Services Secret Key. Found in the AWS Security Credentials.'),
    );
    $form['required']['aws_certificate_authority'] = array(
      '#type' => 'checkbox',
      '#title' => t('Determines which Cerificate Authority file to use'),
      '#default_value' => $config->get('aws_certificate_authority'),
      '#description' => t('A value of boolean `false` will use the Certificate Authority file available on the system. A value of boolean `true` will use the Certificate Authority provided by the SDK. Passing a file system path to a Certificate Authority file (chmodded to `0755`) will use that. Leave this set to `false` if you\'re not sure.'),
    );
    $form['required']['aws_default_cache_config'] = array(
      '#type' => 'textarea',
      '#title' => t('Storage type to use for caching'),
      '#default_value' => $config->get('aws_default_cache_config'),
      '#description' => t('This option allows you to configure a preferred storage type to use for caching by default. Valid values are: `apc`, `xcache`, a DSN-style string such as `pdo.sqlite:/sqlite/cache.db`, a file system path such as `./cache` or `/tmp/cache/`, or a serialized array for memcached configuration.'),
    );

    $form['extra'] = array(
      '#type' => 'details',
      '#title' => t('Extra'),
      '#description' => t('The following fields are never used by the SDK, but can be handy in certain usecases or may be required by some modules.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['extra']['aws_account_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Amazon Account ID without dashes'),
      '#default_value' => $config->get('aws_account_id'),
      '#description' => t('Amazon Account ID without dashes. Used for identification with Amazon EC2. Found in the AWS Security Credentials.'),
    );
    $form['extra']['aws_canonical_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Your CanonicalUser ID'),
      '#default_value' => $config->get('aws_canonical_id'),
      '#description' => t('Your CanonicalUser ID. Used for setting access control settings in AmazonS3. Found in the AWS Security Credentials.'),
    );
    $form['extra']['aws_canonical_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Your CanonicalUser DisplayName'),
      '#default_value' => $config->get('aws_canonical_name'),
      '#return_value' => TRUE,
      '#description' => t('Your CanonicalUser DisplayName. Used for setting access control settings in AmazonS3. Found in the AWS Security Credentials (i.e. "Welcome, AWS_CANONICAL_NAME").'),
    );
    $form['extra']['aws_mfa_serial'] = array(
      '#type' => 'textfield',
      '#title' => t('12-digit serial number'),
      '#default_value' => $config->get('aws_mfa_serial'),
      '#description' => t('12-digit serial number taken from the Gemalto device used for Multi-Factor Authentication. Ignore this if you\'re not using MFA.'),
    );
    $form['extra']['aws_cloudfront_keypair'] = array(
      '#type' => 'textfield',
      '#title' => t('Amazon CloudFront key-pair to use for signing private URLs'),
      '#default_value' => $config->get('aws_cloudfront_keypair'),
      '#description' => t('Amazon CloudFront key-pair to use for signing private URLs. Found in the AWS Security Credentials.'),
    );
    $form['extra']['aws_cloudfront_pem'] = array(
      '#type' => 'textarea',
      '#title' => t('The contents of the *.pem private key that matches with the CloudFront key-pair ID'),
      '#default_value' => $config->get('aws_cloudfront_pem'),
      '#description' => t('The contents of the *.pem private key that matches with the CloudFront key-pair ID. Found in the AWS Security Credentials.'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('awssdk.configuration');
    foreach($form_state->getValues() as $key => $value) {
      if (substr($key, 0, 4) == 'aws_') {
        $config->set($key, $value);
      }
    }
    $config->save();
  }

}
