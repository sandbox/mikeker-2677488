<?php

/**
 * @file
 * Contains \Drupal\awssdk\Awssdk.
 */

namespace Drupal\awssdk;

use Aws\Sdk;
use Aws\Credentials\Credentials;
use Aws\Credentials\CredentialProvider;
use GuzzleHttp\Promise;
use GuzzleHttp\Promise\RejectedPromise;

/**
 * Class Awssdk.
 *
 * @package Drupal\awssdk
 */
class Awssdk extends Sdk {

  /*
   * The AWS credentials can be stored as environment variables.
   */
  const AWSSDK_KEY = 'AWSSDK_KEY';
  const AWSSDK_SECRET = 'AWSSDK_SECRET';

  public static function credentials() {
    // Return a function that is the credential provider.
    return function () {
      // Use credentials from environment variables, if available.
      $key = getenv(self::AWSSDK_KEY);
      $secret = getenv(self::AWSSDK_SECRET);
      if ($key && $secret) {
        return Promise\promise_for(new Credentials($key, $secret));
      }

      // If not, use Drupal config variables. (Automatically handles overrides
      // in settings.php.)
      $key = \Drupal::config('awssdk.configuration')->get('aws_key');
      $secret = \Drupal::config('awssdk.configuration')->get('aws_secret');
      if ($key && $secret) {
        return Promise\promise_for(new Credentials($key, $secret));
      }

      return new RejectedPromise(new CredentialsException(t('Could not find AWS SDK credentials in either environment variables or in Drupal\'s config variables.')));
    };
  }

  public function __construct(array $args = []) {
    $provider = self::credentials();
    // For performance, memoize the provider.
    $provider = CredentialProvider::memoize($provider);

    // Pass the provider into the Sdk class and share the provider across
    // multiple clients. Each time a new client is constructed, it will use the
    // previously returned credentials as long as they have not yet expired.
    parent::__construct(['credentials' => $provider]);
  }

}
